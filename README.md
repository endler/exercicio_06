# chat_app

Exemplo de uma tela de chat.

## Tarefa

Usar image_picker (lembrar de importar no pubspec.yaml) para adicionar uma imagem no fluxo do chat.

### Dicas:

- importe a versão image_picker: 0.4.12+1 no seu pubspec.yaml, pois as versões mais novas usam o AndroidX (um novo sistema de suporte a retrocompatibilidade de versões do android.)
- o image_picker serve tanto para fotos tiradas pela câmera, como também para acessar a galeria do smartphone.
- Para quem está usando o emulador, tente tirar um screenshoot. Assim, sua galeria terá ao menos uma imagem.
- Crie um botão apropriado para invocar o método que chama a câmera.
- Altere a classe "ChatMessage" para que ela possa receber uma imagem também.

